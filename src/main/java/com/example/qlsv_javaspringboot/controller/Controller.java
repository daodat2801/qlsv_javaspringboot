package com.example.qlsv_javaspringboot.controller;

import lombok.RequiredArgsConstructor;
import com.example.qlsv_javaspringboot.model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.qlsv_javaspringboot.service.StudentService;

import java.util.List;

@RestController
@RequestMapping("/studentMGMT")
@RequiredArgsConstructor
public class Controller {
    private final StudentService service;

    @GetMapping("/getAll")
    public List<Student> getAllStudent() {
        return service.getAllStudent();
    }

    @PostMapping("/create")
    public ResponseEntity createNewStudent(@RequestBody Student student){
        return ResponseEntity.ok().body(service.addStudent(student));
    }

    @GetMapping("/get/{name}")
    public List<Student> getAllStudent(@PathVariable(name = "name") String name) {
        return service.getStudentsByName(name);
    }

    @PutMapping("/update")
    public ResponseEntity updateStudentInfo(@RequestBody Student student){
        return ResponseEntity.ok().body(service.updateStudent(student));

    }

    @DeleteMapping("/delete/{studentId}")
    public String deleteStudentInfo(@PathVariable(name = "studentId") Integer studentId){
        service.deleteStudent(studentId);
        return "OK";
    }
}
