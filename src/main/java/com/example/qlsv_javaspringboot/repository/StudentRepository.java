package com.example.qlsv_javaspringboot.repository;

import com.example.qlsv_javaspringboot.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query("select s from Student s where s.name like %?1%")
    List<Student> findAllByName(String name);
}
