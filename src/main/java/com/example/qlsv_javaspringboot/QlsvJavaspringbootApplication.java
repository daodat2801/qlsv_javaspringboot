package com.example.qlsv_javaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QlsvJavaspringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(QlsvJavaspringbootApplication.class, args);

    }

}
