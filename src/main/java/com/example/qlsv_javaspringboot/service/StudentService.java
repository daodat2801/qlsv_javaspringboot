package com.example.qlsv_javaspringboot.service;

import com.example.qlsv_javaspringboot.model.Student;

import java.util.List;

public interface StudentService {
    Student addStudent(Student student);

    Student updateStudent(Student student);

    List<Student> getAllStudent();

    List<Student> getStudentsByName(String name);

    void deleteStudent(int studentId);

}
