package com.example.qlsv_javaspringboot.service;

import lombok.RequiredArgsConstructor;
import com.example.qlsv_javaspringboot.model.Student;
import org.springframework.stereotype.Service;
import com.example.qlsv_javaspringboot.repository.StudentRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    @Override
    public Student updateStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

    @Override
    public List<Student> getStudentsByName(String name) {
        return studentRepository.findAllByName(name);
    }

    @Override
    public void deleteStudent(int studentId) {
        studentRepository.deleteById(studentId);
    }

    @Override
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }
}
